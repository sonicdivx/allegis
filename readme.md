## Requirements

- Page must be responsive
- Use of a grid system or CSS framework is encouraged. Explain why you chose this particular one.
- Page should be compatible with current versions of Chrome, Firefox, Safari and IE
- Open Sans is used for the desktop version
- Form does not have to submit or do anything. Just create the form elements.
- Each title below the section that begins with "Sign-up to view" should function like an accordion. Each title and chevron should be used to toggle the visibility of the content directly below it. When the section is open, the chevron should point upward. When the section is closed, the chevron should point downward.

## Software & Tools
- SmartGit
- Brackets
- Photoshop
- Illustrator
- Node.js (for node modules to compile SASS, if needed) Optional

##### HTML/CSS Framework
Zurb Foundation [Foundation 6](http://foundation.zurb.com/sites/download.html/)

## Choices
- PNG for images (though some were Gifs dues to auto export from PSD)
- local css and js files instead of CDN to ensure viewing of files.
- Chose Foundation because I have found their committment to responsive design is more foward thinking that most frameworks. The ver.6 is new to me but, offers a very flexible approach to the grid system. Though not completely semantic, is easily maintainable and fairly DRY.
- Implementation of Foundation is not semantic. To do that would need to build custom SASS files and use Foundation mixins. 
- CSS files used are not minified, to facilitate easier review while developing.
- Pathing is relative to allow local review.
- Font Awesome does not have a Glassdoor icon. Also Glassdoor icon changed, so used image from mockup to maintain pixel perfection.

## Issues
- XY grid is still needing some updates to certain design elements, such as forms. This required a non-standard Foundation implementation.
- XY grid took much longer to implement and not perfect, This is largely due to having not worked with a "off the shelf" framework on a significant project in six years. (current work has been with custom tailored frameworks)
- Would need to add more breakpoints to address issues in the mid browser viewpoint area. 
- TODO: Header needs refactor, ugh....

## Comments & Questions
- Initial Mockup for desktop element widths in main content block are uneven. Is this intentional?
- Is pixel perfection expected, or should design be adjusted to more easily fit CSS framework grid?
- Icons in mobile version have different colors? Is this intentional?
- Possibly remove gradients and shadows as offer little in terms of UI clarity. Also none existent in mobile, requiring additional CSS to maintain.
- Icon image, if pulled from PSD should be same dimensions (i.e. with adjusted padding). Easier to cut to a fixed size and apply consistent margin and padding. Important for responsive design.
- Social Media buttons are the simple links to respective company "social site" or meant to be share links?
- Google indicates that a font file setup with Open Sans Regular,Bold and Semi-Bold will be slower. Can design be refactored to reduce fonts used?
- Recommend Form fields not all be required. fewer requirements increases click-thru/engagement.
- No hover states defined.
- Accordion panel arrow direction different betwen deskop and mobile design.
- Inconsisten margins and padding on elements.
- Buttons are different styles and too many colors.
- Asset creation from PSD is not optimized, requiring extra work to create variations.
- Currently, the form aside and the headline blocks have odd default behavior. This also may be better address refactoring the design a bit more or creating custom components.

## Next Steps
- Convert all icons into a an icon font. Allows for CSS sizing and gernerally smaller and faster to render. For Multi-color icons, continue to use image
Convert all custom CSS in app.css to SASS using Foundation SASS version.
- Add Form Validation. Design may have to be adjusted to account for validation messages.
- review comoponents and look at making custom ones that work with CSS framework.

